# nerf

> Not your father's logger

[![pipeline status](https://gitlab.com/jrop-js/nerf/badges/master/pipeline.svg)](https://gitlab.com/jrop-js/nerf/commits/master)
[![coverage report](https://gitlab.com/jrop-js/nerf/badges/master/coverage.svg)](https://gitlab.com/jrop-js/nerf/commits/master)

## Features

* Mimics Chrome's `console.*` methods
* Logs to self-contained interactive HTML file

![Sample UI Screenshot](https://gitlab.com/jrop-js/nerf/raw/master/screenshot.png)

## Installation

```
npm install nerf
```

## Usage

```js
import {Logger, Writer} from 'nerf'
const logger = new Logger({
	proxy: true, // also invoke corresponding `console.*` methods
	writer: new Writer('/my/file.html'),
})
logger.log('I can log strings!')
logger.log({I: 'can', log: 'objects!'})

const group = logger.group('My Group')
group.log('In the group')

logger.count('label') // 1
logger.count('label') // 2
logger.count('label') // 3
```

After your program exits, open `/my/file.html` and browse your program's output!

Need more documentation?  [Check out the Wiki](https://gitlab.com/jrop-js/nerf/wikis/Documentation).

## License (ISC)

ISC License (ISC)
Copyright 2017 Jonathan Apodaca <jrapodaca@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
