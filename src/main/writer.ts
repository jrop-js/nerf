import fs = require('mz/fs')
import Logger, {LoggerBundleOptions} from './logger'

export default class Writer {
	file: string
	options: LoggerBundleOptions
	logger: Logger

	private _head: Promise<any>
	private _next: () => Promise<any>

	constructor(file: string, options?: LoggerBundleOptions) {
		this.file = file
		this.options = options
	}

	private async _write() {
		await fs.writeFile(this.file, await this.logger.bundleViewer(this.options))
	}

	tick() {
		this._head = this._next ? this._next() : null
		this._next = null
	}

	scheduleFileUpdate() {
		const queue = () => () => {
			const promise = this._write()
			promise.then(() => this.tick(), () => this.tick())
			return promise
		}

		if (!this._head && !this._next) this._head = queue()()
		else if (this._head) this._next = queue()
	}

	async wait() {
		while (this._head) await this._head
	}
}
