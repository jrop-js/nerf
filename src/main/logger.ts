import {EventEmitter} from 'events'
import fs = require('mz/fs')
import json5 = require('json5')
import path = require('path')
import {
	AssertEntry,
	CountEntry,
	DataEntry,
	GroupEntry,
	LogEntry,
	LogLevel,
	TableEntry,
	TimerEntry,
} from './entries'
import {Customizer, copy, serialize} from './object'
import Writer from './writer'

function defer() {
	const d = {
		promise: new Promise(
			(resolve, reject) => ((d.resolve = resolve), (d.reject = reject))
		),
	}
	return d as {
		resolve: (v) => any
		reject: (e) => any
		promise: Promise<any>
	}
}

export type LoggerOptions = {
	customizer?: Customizer
	proxy?: boolean
	writer?: Writer
}
export type LoggerBundleOptions = {
	lite?: boolean
	minified?: boolean
}

export default class Logger extends EventEmitter {
	entries: LogEntry[]
	private _counters: Map<string, number>
	private _options: LoggerOptions

	constructor(options: LoggerOptions = {}) {
		super()
		this.entries = []
		this._counters = new Map()
		this._options = options
		if (this._options.writer && !this._options.writer.logger)
			this._options.writer.logger = this
	}
	_proxyCall(name, ...args) {
		this.emit(name, ...args)
		if (this._options.writer) this._options.writer.scheduleFileUpdate()
		if (typeof console[name] == 'function' && this._options.proxy)
			console[name](...args)
	}

	proxy(shouldProxy: boolean = true) {
		this._options.proxy = shouldProxy
		return this
	}

	assert(test, ...args) {
		args = copy(args, this._options.customizer)
		if (test) return this
		this.entries.push(new AssertEntry(args))
		this._proxyCall('assert', test, ...args)
		return this
	}
	count(label) {
		if (!this._counters.has(label)) this._counters.set(label, 0)
		this._counters.set(label, this._counters.get(label) + 1)
		this.entries.push(new CountEntry(label, this._counters.get(label)))
		this._proxyCall('count', label)
		return this
	}

	debug(...args) {
		args = copy(args, this._options.customizer)
		this.entries.push(new DataEntry(LogLevel.LOG, 'debug', args))
		this._proxyCall('debug', ...args)
		return this
	}
	error(...args) {
		args = copy(args, this._options.customizer)
		this.entries.push(new DataEntry(LogLevel.ERROR, 'error', args))
		this._proxyCall('error', ...args)
		return this
	}
	info(...args) {
		args = copy(args, this._options.customizer)
		this.entries.push(new DataEntry(LogLevel.INFO, 'info', args))
		this._proxyCall('info', ...args)
		return this
	}
	log(...args) {
		args = copy(args, this._options.customizer)
		this.entries.push(new DataEntry(LogLevel.LOG, 'log', args))
		this._proxyCall('log', ...args)
		return this
	}
	trace(...args) {
		args = copy(args, this._options.customizer)
		this.entries.push(new DataEntry(LogLevel.LOG, 'trace', args))
		this._proxyCall('trace', ...args)
		return this
	}
	warn(...args) {
		args = copy(args, this._options.customizer)
		this.entries.push(new DataEntry(LogLevel.WARN, 'warn', args))
		this._proxyCall('warn', ...args)
		return this
	}

	table(data) {
		data = copy(data, this._options.customizer)
		this.entries.push(new TableEntry(data))
		this._proxyCall('table', data)
		return this
	}
	group(label: string) {
		// no 1-to-1 mapping for this implementation
		// to console.group(): no this._proxy() call here
		const g = new GroupEntry(label, this._options)
		this.emit('group', g)
		this.entries.push(g)
		if (this._options.writer) this._options.writer.scheduleFileUpdate()
		return g.logger
	}

	time(label: string) {
		const t = new TimerEntry(this, label)
		this.entries.push(t)
		this._proxyCall('time', label)
		return t
	}

	toObject(): any {
		return {
			entries: this.entries.map(e => e.toObject()),
		}
	}

	async bundleViewer(
		opts: LoggerBundleOptions = {minified: true}
	): Promise<string> {
		const version = require('../../package').version
		return `<!doctype html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Log Viewer</title>
	<style>
		html, body, #root {
			box-sizing: border-box;
			font-family: sans-serif;
			height: 100%;
			margin: 0;
			padding: 0;
		}
		*, *::after, *::before {
			box-sizing: inherit;
		}
	</style>
</head>
<body>
	<div id="root"></div>
	<script>window.__DATA__ = ${serialize(this.toObject())}</${'script'}>
	${opts.lite
		? `<script src="https://unpkg.com/nerf@${version}/lib/public/index.min.js"></${'script'}>`
		: `<script>${await fs.readFile(
				path.join(
					__dirname,
					'..',
					'public',
					`index${opts.minified ? '.min' : ''}.js`
				),
				'utf8'
			)}</${'script'}>`}
</body>
</html>
		`
	}

	static fromObject(obj) {
		const entries = obj.entries.map(e => LogEntry.fromObject(e))
		return Object.assign(new Logger(), obj, {
			entries,
		})
	}
}
