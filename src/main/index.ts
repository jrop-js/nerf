import Logger from './logger'
import Writer from './writer'

export default Logger
export * from './logger'
export * from './entries'
export {Logger, Writer}
