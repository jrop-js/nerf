import Logger, {LoggerOptions} from './logger'

function captureStack() {
	return new Error().stack
		.split('\n')
		.slice(1) // cut off first line 'Error'
		.filter(ln => !ln.includes(__dirname))
		.join('\n')
}
function captureSourceLine() {
	const [src] = captureStack()
		.split('\n')
		.slice(1) // cut off first line 'Error'
	// `src` is in the form 'at functionLocation (/some/file.js:1:2)'
	return (/^[^(]+\((.*)\)$/.exec(src) || [])[1]
}

export enum LogLevel {
	LOG,
	INFO,
	WARN,
	ERROR,
}

export class LogEntry {
	level: LogLevel
	type: string
	source: string
	time: number
	constructor(level: LogLevel, type: string) {
		this.level = level
		this.type = type
		this.source = captureSourceLine()
		this.time = new Date().getTime()
	}

	toObject(): any {
		const obj = {}
		for (const key of Object.keys(this)) {
			const val = this[key]
			if (val && typeof val.toObject == 'function') obj[key] = val.toObject()
			else obj[key] = val
		}
		return obj
	}

	static fromObject(obj): LogEntry {
		switch (obj.type) {
			case 'count':
				return Object.assign(
					new CountEntry(obj.label, obj.count),
					obj
				) as LogEntry
			case 'table':
				return Object.assign(new TableEntry(obj.data), obj) as LogEntry
			case 'timer':
				return Object.assign(new TimerEntry(null, obj.label), obj) as LogEntry
			case 'group':
				return GroupEntry.fromObject(obj)
			default:
				return Object.assign(
					new DataEntry(obj.level, obj.type, obj.data),
					obj
				) as LogEntry
		}
	}
}

export class DataEntry extends LogEntry {
	data: any
	constructor(level: LogLevel, type: string, data: any) {
		super(level, type)
		this.data = data
	}
}

export class AssertEntry extends DataEntry {
	stack: string
	constructor(args) {
		super(LogLevel.ERROR, 'assert', args)
		this.stack = captureStack()
	}
}

export class CountEntry extends DataEntry {
	label: string
	constructor(label: string, count: number) {
		super(LogLevel.INFO, 'count', count)
		this.label = label
	}
	get count() {
		return this.data as number
	}
	set count(val: number) {
		this.data = val
	}
}

export class GroupEntry extends LogEntry {
	label: string
	logger: Logger
	constructor(label: string, options?: LoggerOptions) {
		super(LogLevel.LOG, 'group')
		this.label = label
		this.logger = new Logger(options)
	}
	static fromObject(obj) {
		const ge = new GroupEntry(obj.label)
		ge.logger = Logger.fromObject(obj.logger)
		return ge
	}
}

export class TableEntry extends DataEntry {
	columns: string[]
	constructor(data: any[]) {
		super(LogLevel.INFO, 'table', data)
		const columns = new Set<string>()
		this.data.forEach(row => Object.keys(row).forEach(key => columns.add(key)))
		this.columns = Array.from(columns)
	}
}

export class TimerEntry extends LogEntry {
	_options: LoggerOptions
	logger: Logger
	label: string
	end: number
	constructor(logger: Logger, label: string, options?: LoggerOptions) {
		super(LogLevel.INFO, 'timer')
		this.logger = logger
		this.label = label
		this._options = options || {}
	}
	stop() {
		this.end = new Date().getTime()
		if (this._options.writer) this._options.writer.scheduleFileUpdate()
		this.logger._proxyCall('timeEnd', this.label)
	}
	toObject() {
		return {
			end: this.end,
			label: this.label,
			level: this.level,
			source: this.source,
			time: this.time,
			type: this.type,
		}
	}
}
