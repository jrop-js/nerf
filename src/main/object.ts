import json5 = require('json5')
import transform from 'lodash/transform'

function isPrimitive(obj) {
	return (
		['boolean', 'function', 'number', 'string'].includes(typeof obj) ||
		obj === null ||
		obj === undefined
	)
}

function allKeys(obj) {
	if (!obj) return []
	const props = []
	let curr = obj
	do {
		Object.getOwnPropertyNames(curr).forEach(k => {
			if (typeof obj[k] != 'function' && k !== '__proto__') props.push(k)
		})
	} while ((curr = Object.getPrototypeOf(curr)))
	return props
}

export type Customizer = (target, parent?, key?) => any
export function copy(obj, customizer: Customizer = x => x) {
	return _copy(obj, customizer, new Map(), null, null)
}
function _copy(
	obj,
	customizer: Customizer,
	seen: Map<any, any>,
	parent: any,
	key: any
) {
	obj = customizer(obj, parent, key)
	if (isPrimitive(obj)) return obj
	if (Array.isArray(obj))
		return obj.map((item, idx) => _copy(item, customizer, seen, obj, idx))

	if (seen.has(obj)) return seen.get(obj)
	const newObj = {}
	seen.set(obj, newObj)
	allKeys(obj).forEach(k => {
		newObj[k] = _copy(obj[k], customizer, seen, obj, k)
	})
	return customizer(newObj, parent, key)
}

function _flatten(obj, index = {map: new Map(), counter: 0}) {
	if (index.counter == 0) obj = copy(obj)
	index.map.set(obj, index.counter)
	index.counter = index.counter + 1
	if (isPrimitive(obj)) return
	for (const k of allKeys(obj)) {
		const v = obj[k]
		if (typeof index.map.get(v) == 'undefined') _flatten(v, index)
		obj[k] = index.map.get(v)
	}
}
export function flatten(obj) {
	const index = {map: new Map(), counter: 0}
	_flatten(obj, index)
	const table = new Array(index.counter)
	const entries = index.map.entries()
	let entry: IteratorResult<any>
	while (!(entry = entries.next()).done) {
		const [v, i] = entry.value
		table[i] = v
	}
	return table
}

export function unflatten(table) {
	table = copy(table)
	for (const obj of table) {
		if (isPrimitive(obj)) continue
		for (const k of allKeys(obj)) {
			const idx = obj[k]
			obj[k] = table[idx]
		}
	}
	return table[0]
}

export function serialize(obj) {
	return json5.stringify(
		flatten(
			copy(obj, t => {
				if (t === undefined) return {__undefined__: true}
				if (t instanceof Error)
					return {
						__error__: true,
						name: t.name,
						message: t.message,
						stack: t.stack,
					}
				if (t && t.constructor && t.constructor.name == 'process') {
					return copy(Object.assign({}, process))
				}
				return t
			})
		)
	)
}

export function deserialize(obj) {
	if (typeof obj == 'string') obj = json5.parse(obj)
	return copy(unflatten(obj), t => {
		if (t && t.__undefined__) return undefined
		if (t && t.__error__) return Object.assign(new Error(), t)
		return t
	})
}
