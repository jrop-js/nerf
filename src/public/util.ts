import React = require('react')

export function between(list, item) {
	const newList = new Array()
	for (let i = 0; i < list.length; ++i) {
		newList.push(React.cloneElement(list[i], {key: i}))
		if (i + 1 != list.length)
			newList.push(React.cloneElement(item, {key: list.length + i}))
	}
	return newList
}
