import React = require('react')
import ReactDOM = require('react-dom')

import {InspectLogger} from './logs'
import {Logger} from '../main'

import {deserialize} from '../main/object'

const App = () => (
	<InspectLogger
		logger={Logger.fromObject(deserialize((window as any).__DATA__))}
	/>
)

ReactDOM.render(<App />, document.getElementById('root'))
