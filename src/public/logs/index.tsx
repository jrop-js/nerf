import React = require('react')
import {Logger} from '../../main'
import * as L from '../../main'

import Inspect from './primitives'
import {between} from '../util'
import {Arrow, Icon, Row, SourceLine} from './row'

export class Assert extends React.Component<
	{entry: L.AssertEntry},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		const {props: {entry}} = this
		return (
			<Row underline red icon="⛔">
				<Arrow
					expanded={this.state.expanded}
					onClick={() => this.setState({expanded: !this.state.expanded})}
				/>
				<SourceLine src={entry.source} />
				Assertion failed:{' '}
				{entry.data.map((a, i) => <Inspect key={i} value={a} />)}
				{this.state.expanded
					? entry.stack.split('\n').slice(1).map((p, i) =>
							<div key={i} style={{paddingLeft: '10px'}}>
								{p}
							</div>
						)
					: null}
			</Row>
		)
	}
}

export class Count extends React.Component<{entry: L.CountEntry}> {
	render() {
		const {props: {entry}} = this
		return (
			<Row underline icon="✐">
				<SourceLine src={entry.source} />
				{entry.label}: <Inspect value={entry.count} />
			</Row>
		)
	}
}

export class Group extends React.Component<
	{entry: L.GroupEntry},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		const {collapsed} = props
		this.state = {expanded: !collapsed}
	}
	render() {
		const {props} = this
		return (
			<Row underline={!this.state.expanded}>
				<div>
					<SourceLine src={props.entry.source} />
					<Arrow
						expanded={this.state.expanded}
						onClick={() => this.setState({expanded: !this.state.expanded})}
					/>
					<b>
						{props.entry.label}
					</b>
					{this.state.expanded
						? <InspectLogger logger={props.entry.logger} />
						: null}
				</div>
			</Row>
		)
	}
}

export class Line extends React.Component<{entry: L.DataEntry}> {
	render() {
		const {props: {entry}} = this
		return (
			<Row
				underline
				red={entry.type == 'error'}
				icon={entry.type == 'error' ? '⛔' : entry.type == 'count' ? '✐' : null}>
				<SourceLine src={this.props.entry.source} />

				{/* Display arguments */}
				{between(
					this.props.entry.data.map((v, i) => <Inspect key={i} value={v} />),
					<span>&nbsp;</span>
				)}
			</Row>
		)
	}
}

export class Table extends React.Component<
	{entry: L.TableEntry},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		let {data, columns} = this.props.entry
		data = Array.isArray(data) ? data : [data]
		const fields = new Set()
		data.forEach(row => Object.keys(row).forEach(k => fields.add(k)))
		columns = columns || Array.from(fields.values())

		return (
			<Row underline>
				<div>
					<Arrow
						expanded={this.state.expanded}
						onClick={() => this.setState({expanded: !this.state.expanded})}
					/>
					<b>
						Table({data.length})
					</b>
				</div>
				{this.state.expanded
					? <table>
							<thead>
								<tr>
									<th>(index)</th>
									{columns.map(f =>
										<th key={f}>
											{f}
										</th>
									)}
								</tr>
							</thead>
							<tbody>
								{data.map((row, i) =>
									<tr key={i}>
										<td>
											{i}
										</td>
										{columns.map(f =>
											<td key={f}>
												{row[f] ? <Inspect value={row[f]} /> : null}
											</td>
										)}
									</tr>
								)}
							</tbody>
						</table>
					: null}
			</Row>
		)
	}
}

export class Timer extends React.Component<{
	entry: L.TimerEntry
}> {
	render() {
		return (
			<Row underline icon="⏱">
				{this.props.entry.label || '(unlabeled)'}:{' '}
				{this.props.entry.end - this.props.entry.time}ms
			</Row>
		)
	}
}

export class InspectLogger extends React.Component<{logger: Logger}> {
	render() {
		return (
			<div style={{fontFamily: 'monospace'}}>
				{this.props.logger.entries.map((e, i) => {
					switch (e.type) {
						case 'assert':
							return <Assert key={i} entry={e as L.AssertEntry} />
						case 'debug':
						case 'error':
						case 'info':
						case 'log':
						case 'trace':
						case 'warn':
							return <Line key={i} entry={e as L.DataEntry} />
						case 'count':
							return <Count key={i} entry={e as L.CountEntry} />
						case 'group':
							return <Group key={i} entry={e as L.GroupEntry} />
						case 'table':
							return <Table key={i} entry={e as L.TableEntry} />
						case 'timer':
							return <Timer key={i} entry={e as L.TimerEntry} />
						default:
							throw new Error('Unknown entry type: ' + e.type)
					}
				})}
			</div>
		)
	}
}
