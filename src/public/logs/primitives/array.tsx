import React = require('react')
import {Arrow} from '../row'

import Inspect from './index'
import InspectArrayChunked from './array-chunked'
import {KeyVal} from './object'

export default class InspectArray extends React.Component<
	{value: any},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		const {value} = this.props
		if (value.length > 100) return <InspectArrayChunked value={value} />
		return (
			<span>
				<Arrow
					expanded={this.state.expanded}
					onClick={() => this.setState({expanded: !this.state.expanded})}
				/>
				{this.state.expanded
					? <span>
							[
							<div style={{paddingLeft: '10px'}}>
								<KeyVal k="length" v={value.length} />
							</div>
							{value.map((v, i) =>
								<div key={i} style={{paddingLeft: '10px'}}>
									<KeyVal k={<Inspect value={i} />} v={v} />
								</div>
							)}
							]
						</span>
					: `Array(${value.length})`}
			</span>
		)
	}
}
