import React = require('react')
import {Arrow} from '../row'

import COLORS from '../colors'
import Inspect from './index'
import InspectArray from './array'

export const KeyVal = ({k, v}) => (
	<span>
		<span style={{color: COLORS.key}}>{k}:</span>
		&nbsp;
		<Inspect value={v} />
	</span>
)

export default class InspectObject extends React.Component<
	{value: any},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		const {value} = this.props
		if (Array.isArray(value)) return <InspectArray value={value} />
		if (value === null) return <span style={{color: COLORS.keyword}}>null</span>
		return (
			<span>
				<Arrow
					expanded={this.state.expanded}
					onClick={() => this.setState({expanded: !this.state.expanded})}
				/>
				{this.state.expanded ? (
					<span>
						{'{'}
						{Object.keys(value).map((key, i) => (
							<div key={i} style={{paddingLeft: '10px'}}>
								<KeyVal k={key} v={value[key]} />,
							</div>
						))}
						{'}'}
					</span>
				) : (
					`Object(${Object.keys(value).length})`
				)}
			</span>
		)
	}
}
