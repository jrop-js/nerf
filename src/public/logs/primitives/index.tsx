import React = require('react')
import Modal from 'react-modal'
import {inspect} from 'util'
import {Arrow} from '../row'
import {between} from '../../util'

import COLORS from '../colors'
import InspectArrayChunked from './array-chunked'
import InspectError from './error'
import InspectObject from './object'
import InspectString from './string'

export default function Inspect({value}) {
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
	switch (typeof value) {
		case 'undefined':
			return <span style={{color: COLORS.keyword}}>undefined</span>
		case 'object':
			if (value === null)
				return <span style={{color: COLORS.keyword}}>null</span>
			if (value.__error__) return <InspectError value={value} />
			return <InspectObject value={value} />
		case 'boolean':
			return <InspectBoolean value={value} />
		case 'number':
			return <InspectNumber value={value} />
		case 'string':
			return <InspectString value={value} />
		case 'symbol':
			return <InspectSymbol value={value} />
		case 'function':
			return <InspectFunction value={value} />
	}
}

export function InspectBoolean({value}) {
	return <span style={{color: COLORS.boolean}}>{value ? 'true' : 'false'}</span>
}
export function InspectNumber({value}) {
	return <span style={{color: COLORS.number}}>{value}</span>
}
export function InspectSymbol({value}) {
	return <span style={{color: COLORS.string}}>{value.toString()}</span>
}
export function InspectFunction({value}) {
	return (
		<span>
			function () {'{'}...{'}'}
		</span>
	)
}
