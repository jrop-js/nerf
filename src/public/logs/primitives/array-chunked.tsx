import React = require('react')
import {Arrow} from '../row'

import {KeyVal} from './object'

function chunk(a: any[]): any[][] {
	a = a.slice()
	const chunks = []
	while (a.length > 0) {
		chunks.push(a.slice(0, 100))
		a.splice(0, 100)
	}
	return chunks
}

class InspectChunk extends React.Component<
	{value: any[]; i: number},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		const {value, i} = this.props
		return (
			<div style={{paddingLeft: '10px'}}>
				<Arrow
					expanded={this.state.expanded}
					onClick={() => this.setState({expanded: !this.state.expanded})}
				/>
				[{i * 100}...{i * 100 + 99}]
				{this.state.expanded
					? <div style={{paddingLeft: '10px'}}>
							{value.map((e, idx) => <div><KeyVal k={i * 100 + idx} v={e} /></div>)}
						</div>
					: null}
			</div>
		)
	}
}

export default class InspectArrayChunked extends React.Component<
	{value: any[]},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		const {value} = this.props
		return (
			<span>
				<Arrow
					expanded={this.state.expanded}
					onClick={() => this.setState({expanded: !this.state.expanded})}
				/>
				Array({value.length})
				{this.state.expanded
					? <span>
							[
							{chunk(value).map((chunk, i) =>
								<InspectChunk value={chunk} i={i} />
							)}
							]
						</span>
					: null}
			</span>
		)
	}
}
