import React = require('react')
import {Arrow} from '../row'

export default class InspectError extends React.Component<
	{value: Error},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}
	render() {
		const {value} = this.props
		return (
			<span>
				<Arrow
					expanded={this.state.expanded}
					onClick={() => this.setState({expanded: !this.state.expanded})}
				/>
				{value.name}: {value.message}
				{this.state.expanded
					? value.stack.split('\n').slice(1).map((p, i) =>
							<div key={i} style={{paddingLeft: '10px'}}>
								{p}
							</div>
						)
					: null}
			</span>
		)
	}
}
