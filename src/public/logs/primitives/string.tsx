import React = require('react')
import Modal = require('react-modal')
import {inspect} from 'util'

import COLORS from '../colors'

class Pre extends React.Component<any, any> {
	pre: HTMLElement
	componentDidMount() {
		this.pre.innerHTML = this.props.html
	}
	componentDidUpdate() {
		this.pre.innerHTML = this.props.html
	}
	render() {
		return <pre ref={r => (this.pre = r)} />
	}
}

export default class InspectString extends React.Component<
	{value: string},
	{expanded: boolean}
> {
	constructor(props) {
		super(props)
		this.state = {expanded: false}
	}

	render() {
		const {value} = this.props
		let span
		if (value.length > 100) {
			const s = inspect(value.substring(0, 100), null, null)
			span = (
				<span style={{color: COLORS.string}}>
					{s.substr(0, s.length - 1)}
					<u
						onClick={() => this.setState({expanded: true})}
						style={{cursor: 'pointer'}}>
						...
					</u>'
				</span>
			)
		} else {
			span = (
				<span style={{color: COLORS.string}}>{inspect(value, null, null)}</span>
			)
		}

		return (
			<span>
				<Modal
					isOpen={this.state.expanded}
					contentLabel="String"
					onRequestClose={() => this.setState({expanded: false})}>
					{this.state.expanded ? (
						<Pre html={this.props.value.replace(/</g, '&lt;')} />
					) : null}
				</Modal>
				{span}
			</span>
		)
	}
}
