import React = require('react')

export const Arrow = ({expanded, onClick}) =>
	<span
		onClick={onClick}
		style={{
			display: 'inline-block',
			fontSize: '0.6rem',
			color: '#6f6f6f',
			marginRight: '2px',
			cursor: 'default',
			transform: expanded ? 'rotateZ(90deg)' : 'none',
		}}>
		▶
	</span>

export const Icon = ({icon}) =>
	<span
		style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center',
			flex: '0 0 auto',
			fontSize: '0.6rem',
			height: '1rem',
			width: '1.3rem',
		}}>
		{icon || ' '}
	</span>

export const Row = props =>
	<div
		style={Object.assign(
			{},
			props.style,
			{
				display: 'flex',
				flexDirection: 'row',
				padding: '5px',
			},
			props.underline
				? {borderBottom: `solid 1px ${props.red ? '#ffd7d7' : '#f0f0f0'}`}
				: null,
			props.red ? {color: 'red', background: '#fff0f0'} : null
		)}>
		<Icon icon={props.icon} />
		<div
			style={{
				flex: '1 1 auto',
			}}>
			{props.children}
		</div>
	</div>

export const SourceLine = ({src}) =>
	<div
		style={{
			float: 'right',
			color: '#545454',
			textDecoration: 'underline',
		}}>
		{src}
	</div>
