export default {
	boolean: '#b2259c',
	circular: '#02ce44',
	number: '#1c00cf',
	string: '#c41a16',
	key: '#8e1e96',
	keyword: '#aa0d91',
}
