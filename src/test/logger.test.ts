import fs = require('mz/fs')
import test = require('tape')
import * as l from '../main'
import {Logger, Writer} from '../main'
import {unflatten, deserialize} from '../main/object'
import _ = require('lodash')

function clean(obj) {
	if (typeof obj != 'object') return obj
	return _.transform(obj, (memo, v, k, o) => {
		if (!['entries', 'data', 'args', 'label', 'type', 'logger'].includes(k))
			return
		memo[k] = Array.isArray(v)
			? v.map(e => clean(e))
			: typeof v == 'object' && v !== null ? clean(v) : v
	})
}

test('Logger', t => {
	t.plan(4)
	t.deepLooseEqual(
		new Logger().toObject(),
		{
			entries: [],
		},
		'be empty'
	)

	let rec = new Logger().log('Hello').toObject()
	t.equal(rec.entries.length, 1, 'be of length 1')
	t.equal(rec.entries[0].type, 'log', 'be of type log')
	t.deepLooseEqual(rec.entries[0].data, ['Hello'], 'have one argument')
})

test('Writer', async t => {
	t.plan(2)

	const f = `${__dirname}/.test.html`
	const read = async () => {
		const s = await fs.readFile(f, 'utf-8')
		const [, json] = /<script>window\.__DATA__\s*=\s*(.*?)<\/script>/.exec(s)
		return clean(deserialize(json))
	}
	const writer = new Writer(f, {lite: true})
	const l = new Logger({writer})
	l.log('Hello World!')

	await writer.wait()
	t.deepLooseEqual(
		await read(),
		{
			entries: [
				{
					type: 'log',
					data: ['Hello World!'],
				},
			],
		},
		'file should have 1 log'
	)

	const g = l.group('My Group:')
	g.log('In group')
	await writer.wait()
	t.deepLooseEqual(
		await read(),
		{
			entries: [
				{
					type: 'log',
					data: ['Hello World!'],
				},
				{
					type: 'group',
					label: 'My Group:',
					logger: {
						entries: [
							{
								type: 'log',
								data: ['In group'],
							},
						],
					},
				},
			],
		},
		'file should have 2 logs'
	)
})
