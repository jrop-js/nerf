import test = require('tape')
import {copy, flatten, unflatten} from '../main/object'

test('copy', t => {
	t.plan(2)

	const obj = {
		num: 1,
		child: {
			num: 1,
		},
	}
	const obj2 = copy(obj)
	t.deepLooseEqual(obj, obj2)

	obj.num = 2
	obj.child.num = 2
	t.deepLooseEqual(obj2, {
		num: 1,
		child: {
			num: 1,
		},
	})
})

test('copy (customizer)', t => {
	t.plan(1)

	t.deepLooseEqual(
		copy(
			{x: undefined, y: 'z'},
			t => (t === undefined ? {__undefined__: true} : t)
		),
		{x: {__undefined__: true}, y: 'z'}
	)
})

test('flatten', t => {
	t.plan(1)

	const obj = {child: {}}
	;(obj.child as any).parent = obj
	t.deepLooseEqual(flatten(obj), [
		{
			child: 1,
		},
		{
			parent: 0,
		},
	])
})

test('unflatten', t => {
	t.plan(1)

	const obj = unflatten([
		{
			child: 1,
		},
		{
			parent: 0,
		},
	])
	t.equal(obj, obj.child.parent)
})
