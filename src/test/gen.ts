import fs = require('fs')
import {Logger} from '../main'

const child: any = {}
const parent: any = {}
child.parent = parent
parent.child = child

const l = new Logger()
l.on('error', e => null)

let i = 0
while (i++ < 5) l.count('tick')

l.log('Hello world!')
l.log(
	new Array(100)
		.fill(0)
		.map(x => 'Long string')
		.join('\n')
)
l.log('An object:', {
	Hello: 'World',
	process,
})
l.log('Circular references:', parent)
l.log({key: undefined})
l.log('An array:', [1, undefined, {key: 'value'}, true])

l.group('My title:').log('In group')

l.table([
	{
		f: 'Jon',
		l: 'Doe',
	},
	{
		f: 'Jane',
		l: 'Doe',
	},
	{
		f: 'Joe',
		m: 'Jimmy',
		l: 'Doe',
	},
])

l.assert(false, 'an assertion error')
l.error(new Error('an error'))

const timer = l.time('myLabel')
setTimeout(() => {
	timer.stop()
	l
		.bundleViewer({lite: true})
		.then(html => fs.writeFileSync(`${__dirname}/logs.html`, html))
}, 1000)
