const builder = require('webpack-configify').default

const config = prod =>
	builder()
		.development(!prod)
		.production(prod)
		.src('src/public/index.tsx')
		.loader(['.ts', '.tsx'], 'awesome-typescript-loader', {
			configFileName: `${__dirname}/src/public/tsconfig.json`,
		})
		.merge({
			node: {
				fs: 'empty',
			},
			output: {
				path: `${__dirname}/lib/public`,
				filename: `index${prod ? '.min' : ''}.js`,
			},
			resolve: {
				alias: !prod
					? {}
					: {
							react: 'preact-compat',
							'react-dom': 'preact-compat',
						},
				extensions: ['.js', '.json', '.ts', '.tsx'],
			},
			target: 'web',
		})
		.build()
module.exports = [config(true), config(false)]
if (require.main === module) {
	console.log(inspect(config, null, null))
}
